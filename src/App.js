import "./App.css";
import "antd/dist/antd.css";
import { Input, List, Typography } from "antd";
import { useMutation, useQuery, useQueryClient } from "react-query";
import { addTodo, getTodos } from "./service/api";
import { useState } from "react";

let bgArr = [""];

function App() {
  const [todoTitle, setTodoTitle] = useState('')

  const queryClient = useQueryClient();

  //data: todos => rename
  let { isLoading, data: todos, isError, isFetching } = useQuery("todos", getTodos);

  const addTodoMutation = useMutation(addTodo, {
    onSuccess: () => { 
      //khi add thành công thì làm mất chức năng cache của API getTodos (và sẽ gọi lại API)
      queryClient.invalidateQueries('todos')
     },
  });

  let handleOnChange = (e) => {
    let value = e.target.value;
    console.log("value: ", value);
    setTodoTitle(value)
  };

  let handleAddTodo = () => { 
    let todo = {
      name: todoTitle,
      isComplete: false,

    };
    addTodoMutation.mutate(todo)
   };

  let renderContent = () => {
    if (isFetching) {
      return <h2 className="text-center">Loading ... 😪</h2>;
    }
    if (isError) {
      return <h2 className="text-center">Not available 😟</h2>;
    }

    return (
      <List
        header={<div>Todo List</div>}
        footer={<div>Footer</div>}
        bordered
        dataSource={todos}
        renderItem={(item, index) => {
          let n = index % 4;
          let bg_class = `bg-myColor-${n}`;

          return (
            <List.Item className={bg_class}>
              <Typography.Text mark>[ITEM]</Typography.Text>{" "}
              <p className="text-white">{item.name}</p>
            </List.Item>
          );
        }}
      />
    );
  };

  return (
    <div className="container mx-auto py-5 space-y-5">
      <h1 className="text-3xl font-bold underline text-red-500">
        Hello world!
      </h1>

      <Input size="large" onChange={handleOnChange} placeholder="Basic usage" />
      <button onClick={handleAddTodo} className="px-5 py-2 bg-green-400">Add Todo</button>

      {renderContent()}
    </div>
  );
}

export default App;
