import axios from "axios";

const https = axios.create({
  baseURL: "https://62db6cb7e56f6d82a772862e.mockapi.io",
});

export const getTodos = async () => {
  let response = await https.get("/todos");
  return response.data;
};

export const addTodo = async (todo) => {
  return await https.post("todos", todo);
};
