/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/**/*.{js,jsx,ts,tsx}",
  ],
  theme: {
    extend: {
      colors: {
        'myColor-0': '#cdb4db',
        'myColor-1': '#ffc8dd',
        'myColor-2': '#bde0fe',
        'myColor-3': '#e9c46a',
      }
    },
  },
  plugins: [],
}